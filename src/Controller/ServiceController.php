<?php

/**
 * @file
 * Contains \Drupal\service_manager\Controller\ServiceController.
 */

namespace Drupal\service_manager\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Controller for displaying the list of services.
 */
class ServiceController extends ControllerBase {

  /**
   * @var \Symfony\Component\DependencyInjection\ContainerInterface
   */
  protected $container;

  /**
   * {@inheritdoc}
   */
  public function __construct($container) {
    $this->container = $container;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static($container);
  }

  public function getList() {
    $container = $this->container;
    foreach ($container->getServiceIds() as $id) {
      $service = $container->get($id);
      $service_class = get_class($service);
      if (!is_object($service)) {
        continue;
      }
      $interfaces = class_implements($service);
        $list[$service->_serviceId] = array(
          '#type' => 'details',
          '#title' => $service->_serviceId,
          '#collapsible' => TRUE,
          '#collapsed' => FALSE,
          '#attributes' => array(),
        );
      $list[$service->_serviceId][] = array(
        '#type' => 'item',
        '#markup' => 'Class: ' . $service_class,
      );
      if (!empty($interfaces)) {
        $interface = current($interfaces);
        $alt_services = $this->getImplementedClasses($interface);
        if (count($alt_services) <= 1) {
          continue;
        }
        $list[$service->_serviceId]['alternatives'] = array(
          '#type' => 'details',
          '#title' => 'Alternative classes (implements '. $interface . ')' ,
          '#collapsible' => TRUE,
          '#collapsed' => FALSE,
          '#attributes' => array(),
        );
        foreach ($alt_services as $class) {
          if ($class !== $service_class) {
            $list[$service->_serviceId]['alternatives'][] = array(
              '#type' => 'item',
              '#markup' => $class
            );
          }
        }
      }
    }
    return $list;
  }

  protected function getImplementedClasses($interface) {
    $classes = get_declared_classes();
    $implements = array();
    foreach($classes as $class) {
      $reflect = new \ReflectionClass($class);
      if($reflect->implementsInterface($interface))
        $implements[] = $class;
    }
    return $implements;
  }

}